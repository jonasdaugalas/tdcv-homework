clear
clc
close all
addpath('helper_functions')

%% Settings
VLFEAT_ROOT_PATH = '/opt/vlfeat'; % Installation root of VLFeat
LOAD_SIFT_MATCHES = false; % Load saved or recompute

%% Given vars
focal_length = [2960.37845, 2960.37845];
principal_point = [1841.68855, 1235.23369];
image_size = [3680, 2456];

%% Setup
% path to the images folder
path_img_dir = '../data/detection';
% path to object ply file
object_path = '../data/teabox.ply';

% Read the object's geometry 
% Here vertices correspond to object's corners and faces are triangles
[vertices, faces] = read_ply(object_path);

% Load the SIFT model from the previous task
load('sift_model.mat');

camera_params = cameraIntrinsics(focal_length, principal_point, image_size);
intrinsic_matrix = camera_params.IntrinsicMatrix';

%% Get all filenames in images folder

FolderInfo = dir(fullfile(path_img_dir, '*.JPG'));
Filenames = fullfile(path_img_dir, {FolderInfo.name} );
num_files = length(Filenames);
img_indices = 1:24;

%% Match SIFT features of new images to the SIFT model with features computed in the task 1
% You should use VLFeat function vl_ubcmatch()

run(fullfile(VLFEAT_ROOT_PATH, 'toolbox/vl_setup'));
% Place SIFT keypoints and descriptors of new images here
keypoints=cell(num_files,1);
descriptors=cell(num_files,1);
% Place matches between new SIFT features and SIFT features from the SIFT
% model here
sift_matches=cell(num_files,1);

if LOAD_SIFT_MATCHES
    load('sift_matches.mat');
    load('detection_keypoints.mat');
    load('detection_descriptors.mat');
else
    
    % Default threshold for SIFT keypoints matching: 1.5
    % When taking higher value, match is only recognized if similarity is very high
    threshold_ubcmatch = 2;
    
    for i=img_indices
        fprintf('Calculating and matching sift features for image: %d \n', i)
        img = single(rgb2gray(imread(char(Filenames(i)))));
        [keypoints{i}, descriptors{i}] = vl_sift(img) ;
        fprintf('  Num sift keypoints: %d \n', size(keypoints{i}, 2));
        % Match features between SIFT model and SIFT features from new image
        sift_matches{i} = vl_ubcmatch(descriptors{i}, model.descriptors, threshold_ubcmatch);
        fprintf('  Num matches: %d \n', size(sift_matches{i}, 2));
    end
    
    % Save sift features, descriptors and matches and load them when you rerun the code to save time
    save('sift_matches.mat', 'sift_matches');
    save('detection_keypoints.mat', 'keypoints')
    save('detection_descriptors.mat', 'descriptors')
end

num_sift = zeros(num_files, 1);
num_matches = zeros(num_files, 1);
for i=1:num_files
    num_sift(i) = size(keypoints{i}, 2);
    num_matches(i) = size(sift_matches{i}, 2);
end

%% PnP and RANSAC 
% Implement the RANSAC algorithm featuring also the following arguments:
% Reprojection error threshold for inlier selection - 'threshold_ransac'  
% Number of RANSAC iterations - 'ransac_iterations'

% Pseudocode
% i Randomly select a sample of 4 data points from S and estimate the pose using PnP.
% ii Determine the set of data points Si from all 2D-3D correspondences 
%   where the reprojection error (Euclidean distance) is below the threshold (threshold_ransac). 
%   The set Si is the consensus set of the sample and defines the inliers of S.
% iii If the number of inliers is greater than we have seen so far,
%   re-estimate the pose using Si and store it with the corresponding number of inliers.
% iv Repeat the above mentioned procedure for N iterations (ransac_iterations).

% For PnP you can use estimateWorldCameraPose() function
% but only use it with 4 points and set the 'MaxReprojectionError' to the
% value of 10000 so that all these 4 points are considered to be inliers

% Place camera orientations, locations and best inliers set for every image here
cam_in_world_orientations = zeros(3,3,num_files);
cam_in_world_locations = zeros(1,3,num_files);
best_inliers_set = cell(num_files, 1);

num_samples = 4;
fprintf('Min number of sift matches: %d \n', min(num_matches(num_matches>0)));

ransac_iterations = 100;
threshold_ransac = 3.5;

for i = img_indices
    fprintf('Running PnP+RANSAC for image: %d \n', i)

    n_sift_matches = size(sift_matches{i}, 2);
    
    matched_img_coords = keypoints{i}(1:2, sift_matches{i}(1,:)); % 2xN
    matched_3d_coords = model.coord3d(sift_matches{i}(2,:),:); % Nx3
    matched_3d_homo_coords = [matched_3d_coords ones(n_sift_matches, 1)]'; % 4xN
    num_fail_estimate_pose = 0;
    
    for ransac_i = 1:ransac_iterations
        select_samples = randperm(n_sift_matches, num_samples);
        sample_img_points = matched_img_coords(:,select_samples)'; % 4x2
        sample_3d_coords = matched_3d_coords(select_samples,:); % 4x3

        try
            [cam_orientation, cam_location] = estimateWorldCameraPose(...
                sample_img_points, sample_3d_coords, camera_params, 'MaxReprojectionError', 10000);
        catch err
            num_fail_estimate_pose = num_fail_estimate_pose + 1;
            continue
        end
        P = intrinsic_matrix * [cam_orientation -cam_orientation*cam_location'];
        
        projections_homo = P* matched_3d_homo_coords;
        % convert from homo: take x,y, and divide element-wise by z
        % (z is duplicated to match dimensions)
        projections = projections_homo(1:2,:) ./ repmat(projections_homo(3,:),2,1);
        distances = sqrt(sum((projections - matched_img_coords).^2, 1));
        if sum(distances<=threshold_ransac) > size(best_inliers_set{i})  % found more inliers?
            best_inliers_set{i} = find(distances<=threshold_ransac);
            if sum(distances<=threshold_ransac) > 3
                [cam_orientation, cam_location] = estimateWorldCameraPose(...
                    matched_img_coords(:,best_inliers_set{i})',...
                    matched_3d_coords(best_inliers_set{i},:), ...
                    camera_params, 'MaxReprojectionError', threshold_ransac);
            end
            cam_in_world_orientations(:,:,i) = cam_orientation;
            cam_in_world_locations(:,:,i) = cam_location;
            
        end
        
    end
    
    fprintf('  Could not estimate camera pose %d/%d times.\n', num_fail_estimate_pose, ransac_iterations);
end

%% Visualize inliers and the bounding box

% You can use the visualizations below or create your own one
% But be sure to present the bounding boxes drawn on the image to verify
% the camera pose

edges = [[1, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 7]
    [2, 4, 5, 3, 6, 4, 7, 8, 6, 8, 7, 8]];

for i = img_indices
    figure()
    imshow(char(Filenames(i)), 'InitialMagnification', 'fit')
    title(sprintf('Image: %d', i))
    hold on
    
%   Plot inliers set
%    PlotInlierOutlier(best_inliers_set{i}, camera_params, sift_matches{i}, model.coord3d, keypoints{i}, cam_in_world_orientations(:,:,i), cam_in_world_locations(:,:,i))
%   Plot bounding box
    points = project3d2image(vertices',camera_params, cam_in_world_orientations(:,:,i), cam_in_world_locations(:, :, i));
    for j=1:12
        plot(points(1, edges(:, j)), points(2, edges(:,j)), 'color', 'b');
    end
    hold off;
end