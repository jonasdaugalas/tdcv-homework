%% [Task 2] debug sift matches
t2_filenames = fullfile('../data/detection', {dir(fullfile('../data/detection', '*.JPG')).name} );
i = 21;

figure()
hold on;
imshow(char(t2_filenames(i)), 'InitialMagnification', 'fit');
vl_plotframe(keypoints{i}(:, sift_matches{i}(1,:)), 'linewidth',2);
title('SIFT matches')
hold off;

figure()
scatter3(model.coord3d(sift_matches{i}(2,:),1), model.coord3d(sift_matches{i}(2,:),2), model.coord3d(sift_matches{i}(2,:),3), 'o', 'b');
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');

%% [Task 1] debug 2d-3d correspondances
load('debug.mat')
t1_filenames = fullfile('../data/init_texture', {dir(fullfile('../data/init_texture', '*.JPG')).name} );
i = 7;

figure()
hold on;
imshow(char(t1_filenames(i)), 'InitialMagnification', 'fit');
vl_plotframe(debug.model_keypoints{i}(:,:), 'linewidth',2);
title('SIFT features')
hold off;

figure()
scatter3(debug.model_coord3d{i}(:,1), debug.model_coord3d{i}(:,2), debug.model_coord3d{i}(:,3), 'o', 'b');
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');