%% Clean
clear
clc
close all
addpath('helper_functions')

%% Settings
LOAD_POINT_LABELS = true; % Load saved or relabel
DISPLAY_CORNER_LABELS = false;
LOAD_SIFT_DESCRIPTORS = true; % Load saved or recompute
VLFEAT_ROOT_PATH = '/opt/vlfeat'; % Installation root of VLFeat

%% Given vars
focal_length = [2960.37845, 2960.37845];
principal_point = [1841.68855, 1235.23369];
image_size = [3680, 2456];

%% Setup
% path to the images folder
path_img_dir = '../data/init_texture';
% path to object ply file
object_path = '../data/teabox.ply';
% Read the object's geometry
% Here vertices correspond to object's corners and faces are triangles
[vertices, faces] = read_ply(object_path);
num_faces = size(faces, 1);
% Coordinate System is right handed and placed at lower left corner of tea
% box. Using coordinates of teabox model, vertex numbering is visualized in
% image vertices.png
%imshow('vertices.png')
%title('Vertices numbering')

%% Label images
% You can use this function to label corners of the model on all images
% This function will give an array with image coordinates for all points
% Be careful that some points may not be visible on the image and so this
% will result in NaN values in the output array
% Don't forget to filter NaNs later
num_points = 8;
if LOAD_POINT_LABELS
    load('labeled_points.mat')
else
    labeled_points = mark_image(path_img_dir, num_points);
    % Save labeled points and load them when you rerun the code to save time
    save('labeled_points.mat', 'labeled_points');
end

%% Get all filenames in images folder
FolderInfo = dir(fullfile(path_img_dir, '*.JPG'));
Filenames = fullfile(path_img_dir, {FolderInfo.name} );
num_files = length(Filenames);

%% Check corners labeling by plotting labels
if DISPLAY_CORNER_LABELS
    for i=1:num_files
        figure()
        imshow(char(Filenames(i)), 'InitialMagnification', 'fit')
        title(sprintf('Image: %d', i))
        hold on
        for point_idx = 1:num_points
            x = labeled_points(point_idx,1,i);
            y = labeled_points(point_idx,2,i);
            if ~isnan(x)
                plot(x,y,'x', 'LineWidth', 3, 'MarkerSize', 15)
                text(x,y, char(num2str(point_idx)), 'FontSize',12)
            end
        end
    end
end

%% Group visible points per image
visible_image_points = cell(num_files);
visible_vertices = cell(num_files);
for i=1:num_files
    [visible_image_points{i}, removed] = rmmissing(labeled_points(:,:,i));
    visible_vertices{i} = vertices(~removed,:);
end

%% Call estimateWorldCameraPose to perform PnP
% Place estimated camera orientation and location here to use
% visualisations later
cam_in_world_orientations = zeros(3,3,num_files);
cam_in_world_locations = zeros(1,3,num_files);

camera_params = cameraIntrinsics(focal_length, principal_point, image_size);
max_reproj_err = 3;

for i=1:num_files
    fprintf('Estimating pose for image: %d \n', i)
    [cam_in_world_orientations(:,:,i),cam_in_world_locations(:,:,i)] = estimateWorldCameraPose(visible_image_points{i}, visible_vertices{i}, camera_params, 'MaxReprojectionError', max_reproj_err);
end

%% Visualize computed camera poses

% Edges of the object bounding box
edges = [[1, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 7]
    [2, 4, 5, 3, 6, 4, 7, 8, 6, 8, 7, 8]];
visualise_cameras(vertices, edges, cam_in_world_orientations, cam_in_world_locations);

%% Detect SIFT keypoints in the images

% You will need vl_sift() and vl_plotframe() functions
% download vlfeat (http://www.vlfeat.org/download.html) and unzip it somewhere
% Don't forget to add vlfeat folder to MATLAB path
% Place SIFT keypoints and corresponding descriptors for all images here
run(fullfile(VLFEAT_ROOT_PATH, 'toolbox/vl_setup'));

keypoints = cell(num_files,1);
descriptors = cell(num_files,1);

if LOAD_SIFT_DESCRIPTORS
    load('sift_descriptors.mat');
    load('sift_keypoints.mat');
else
    for i=1:num_files
        fprintf('Calculating sift features for image: %d \n', i);
        img = single(rgb2gray(imread(char(Filenames(i)))));
        [keypoints{i}, descriptors{i}] = vl_sift(img) ;
    end
    % Save sift features and descriptors and load them when you rerun the code to save time
    save('sift_descriptors.mat', 'descriptors')
    save('sift_keypoints.mat', 'keypoints')
end

num_sift = zeros(num_files, 1);
for i=1:num_files
    num_sift(i) = size(keypoints{i}, 2);
end

% Visualisation of sift features for the first image
figure()
hold on;
imshow(char(Filenames(1)), 'InitialMagnification', 'fit');
vl_plotframe(keypoints{1}(:,:), 'linewidth',2);
title('SIFT features')
hold off;

%% Group coordinates per face (triangle)

% tri_coords: 12x3x3 (n_faces x vtx_per_face x coords_per_vtx)
tri_coords = NaN(num_faces, 3 ,3);
for i=1:num_faces
    face_corners = faces(i,:) + 1;
    tri_coords(i,:,:) = vertices(face_corners(:),:);
end
vtx_x = squeeze(tri_coords(:,1,:));
vtx_y = squeeze(tri_coords(:,2,:));
vtx_z = squeeze(tri_coords(:,3,:));

%% Build SIFT model
% Filter SIFT features that correspond to the features of the object

% Project a 3D ray from camera center through a SIFT keypoint
% Compute where it intersects the object in the 3D space
% You can use TriangleRayIntersection() function here

% Your SIFT model should only consists of SIFT keypoints that correspond to
% SIFT keypoints of the object
% Don't forget to visualise the SIFT model with the respect to the cameras
% positions

% num_samples - number of SIFT points that is randomly sampled for every image
% Leave the value of 1000 to retain reasonable computational time for debugging
% In order to contruct the final SIFT model that will be used later, consider
% increasing this value to get more SIFT points in your model
num_samples=min(num_sift);

% Place model's SIFT keypoints coordinates and descriptors here
model.coord3d = [];
model.descriptors = [];

debug.model_coord3d = cell(num_files, 1);   % to visualize per image 3d correspondances
debug.model_keypoints = cell(num_files, 1); % to visualize filtered keypoints

intrinsic_matrix = transpose(camera_params.IntrinsicMatrix); % transpose to match convention used in lecture material

for i=1:num_files

    % Randomly select a number of SIFT keypoints
    sel = randperm(size(keypoints{i},2), num_samples);

    C = cam_in_world_locations(:,:,i)'; % == -invQ*q;
    % Projection matrix = [Q | q] = intrinsics * [R | t]
    P = intrinsic_matrix*[cam_in_world_orientations(:,:,i) -cam_in_world_orientations(:,:,i)*C];
    Q = P(:,1:3);
    q = P(:,4);
    invQ = inv(Q);
    
    repeated_cam_locs = repmat(cam_in_world_locations(:,:,i), num_faces, 1); % world location repeated num_faces times for intersection

    for sample_idx = 1:num_samples
        x = keypoints{i}(1,sel(sample_idx)); % first row - X coord of SIFT keypoint
        y = keypoints{i}(2,sel(sample_idx)); % second row - Y coord of SIFT keypoint
        direction = invQ * [x; y; 1]; % ray direction
        
        % Find intersections of a ray for all face triangles
        [intersects, distances, u, v, intersect_coords] = TriangleRayIntersection(...
            repeated_cam_locs, ... 
            repmat(transpose(direction), num_faces, 1), ... % ray direction repeated num_faces times
            vtx_x, vtx_y, vtx_z);
        
        if nnz(intersects) > 0  % Found some intersections
            % Only the intersection with the shortest distance is visible
            distances(~intersects) = NaN;
            [min_dist, idx_min_dist] = min(distances);
            model.coord3d = [model.coord3d; intersect_coords(idx_min_dist,:)];
            model.descriptors = [model.descriptors descriptors{i}(:,sel(sample_idx))]; 
            
            debug.model_coord3d{i} = [debug.model_coord3d{i}; intersect_coords(idx_min_dist,:)];
            debug.model_keypoints{i} = [debug.model_keypoints{i} keypoints{i}(:,sel(sample_idx))];
        end
    end
end

% Visualise cameras and model SIFT keypoints
fig = visualise_cameras(vertices, edges, cam_in_world_orientations, cam_in_world_locations);
hold on
scatter3(model.coord3d(:,1), model.coord3d(:,2), model.coord3d(:,3));
hold off
xlabel('x');
ylabel('y');
zlabel('z');

% Save your sift model for the future tasks
save('sift_model.mat', 'model');
% Save debug data for visualization
save('debug.mat', 'debug');


%% Visualise only the SIFT model
figure()
scatter3(model.coord3d(:,1), model.coord3d(:,2), model.coord3d(:,3), 'o', 'b');
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');