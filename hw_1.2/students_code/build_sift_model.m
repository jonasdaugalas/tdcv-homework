function [model]=build_sift_model(i,cam_in_world_orientations,cam_in_world_locations,...
    keypoints,descriptors,vtx_x,vtx_y,vtx_z,camera_params,num_faces)

    %Initialization
    num_samples=min(size(keypoints{i}, 2));
    intrinsic_matrix = transpose(camera_params.IntrinsicMatrix);
    
    model.coord3d = [];
    model.descriptors = [];
    % Randomly select a number of SIFT keypoints
    sel = randperm(size(keypoints{i},2), num_samples);

    C = cam_in_world_locations(:,:,i)'; % == -invQ*q;
    % Projection matrix = [Q | q] = intrinsics * [R | t]
    P = intrinsic_matrix*[cam_in_world_orientations(:,:,i) -cam_in_world_orientations(:,:,i)*C];
    Q = P(:,1:3);
    q = P(:,4);
    invQ = inv(Q);
    
    repeated_cam_locs = repmat(cam_in_world_locations(:,:,i), num_faces, 1); % world location repeated num_faces times for intersection

    for sample_idx = 1:num_samples
        x = keypoints{i}(1,sel(sample_idx)); % first row - X coord of SIFT keypoint
        y = keypoints{i}(2,sel(sample_idx)); % second row - Y coord of SIFT keypoint
        direction = invQ * [x; y; 1]; % ray direction
        
        % Find intersections of a ray for all face triangles
        [intersects, distances, u, v, intersect_coords] = TriangleRayIntersection(...
            repeated_cam_locs, ... 
            repmat(transpose(direction), num_faces, 1), ... % ray direction repeated num_faces times
            vtx_x, vtx_y, vtx_z);
        
        if nnz(intersects) > 0  % Found some intersections
            % Only the intersection with the shortest distance is visible
            distances(~intersects) = NaN;
            [min_dist, idx_min_dist] = min(distances);
            model.coord3d = [model.coord3d; intersect_coords(idx_min_dist,:)];
            model.descriptors = [model.descriptors descriptors{i}(:,sel(sample_idx))]; 
            
%             debug.model_coord3d{i} = [debug.model_coord3d{i}; intersect_coords(idx_min_dist,:)];
%             debug.model_keypoints{i} = [debug.model_keypoints{i} keypoints{i}(:,sel(sample_idx))];
        end
    end
end
