function [jacob] = eval_numeric_gradient(...
    world_coords, expected_img_coords, camera_params, rotation_matrix, location_vector)

rotation_v = rotationMatrixToVector(rotation_matrix);
%residuals = get_residuals(world_coords, expected_img_coords, camera_params, rotation_matrix, location_vector);
jacob = zeros(2*size(world_coords, 1), 6);

for i=1:6
    param = get_param(i, rotation_v, location_vector);
    x = [param-1e-9 param param+1e-9];
    [forward_rotation_v, forward_location_v] = set_param(x(3), i, rotation_v, location_vector);
    [backward_rotation_v, backward_location_v] = set_param(x(1), i, rotation_v, location_vector);
    h = x(3) - x(1);
    
    forward_rotation_matrix = rotationVectorToMatrix(forward_rotation_v);
    backward_rotation_matrix = rotationVectorToMatrix(backward_rotation_v);
    val_forward = get_residuals(world_coords, expected_img_coords, camera_params, forward_rotation_matrix, forward_location_v);
    val_backward = get_residuals(world_coords, expected_img_coords, camera_params, backward_rotation_matrix, backward_location_v);
    dFCentral = (val_forward - val_backward)./h;
    jacob(:,i) = dFCentral;
end

end

function [new_rotation_v, new_location]=set_param(val, i, rotation_v, location_vector)
new_rotation_v = rotation_v;
new_location = location_vector;
if i < 4
    new_rotation_v(i) = val;
else
    new_location(i-3) = val;
end
end

function p=get_param(i, rotation_v, location_vector)
if i < 4
    p = rotation_v(i);
else
    p = location_vector(i-3);
end
end

