function [cam_in_world_orients,cam_in_world_locs,best_inliers_set]=ransac(...
    sift_matches, model, keypoints, camera_params)

cam_in_world_orients = zeros(3,3);
cam_in_world_locs = zeros(1,3);
best_inliers_set = zeros(1,0);
num_samples = 4;
ransac_iterations = 250;
threshold_ransac = 3.2;
intrinsic_matrix = camera_params.IntrinsicMatrix';
n_sift_matches = size(sift_matches, 2);
matched_img_coords = keypoints(1:2, sift_matches(1,:)); % 2xN
matched_3d_coords = model.coord3d(sift_matches(2,:),:); % Nx3
matched_3d_homo_coords = [matched_3d_coords ones(n_sift_matches, 1)]'; % 4xN
num_fail_estimate_pose = 0;

for ransac_i = 1:ransac_iterations
    select_samples = randperm(n_sift_matches, num_samples);
    sample_img_points = matched_img_coords(:,select_samples)'; % 4x2
    sample_3d_coords = matched_3d_coords(select_samples,:); % 4x3
    
    try
        [cam_orientation, cam_location] = estimateWorldCameraPose(...
            sample_img_points, sample_3d_coords, camera_params, 'MaxReprojectionError', 10000);
    catch err
        num_fail_estimate_pose = num_fail_estimate_pose + 1;
        continue
    end
    P = intrinsic_matrix * [cam_orientation -cam_orientation*cam_location'];
    
    projections_homo = P* matched_3d_homo_coords;
    % convert from homo: take x,y, and divide element-wise by z
    % (z is duplicated to match dimensions)
    projections = projections_homo(1:2,:) ./ repmat(projections_homo(3,:),2,1);
    distances = sqrt(sum((projections - matched_img_coords).^2, 1));
    if sum(distances<=threshold_ransac) > size(best_inliers_set)  % found more inliers?
        best_inliers_set = find(distances<=threshold_ransac);
    end
end

fprintf('[RANSAC] Best inliers: %d. Could not estimate camera pose %d/%d times.\n', ...
    size(best_inliers_set, 2), num_fail_estimate_pose, ransac_iterations);

[cam_in_world_orients, cam_in_world_locs] = estimateWorldCameraPose(...
    matched_img_coords(:,best_inliers_set)',...
    matched_3d_coords(best_inliers_set,:), ...
    camera_params, 'MaxReprojectionError', ceil(threshold_ransac));
end

