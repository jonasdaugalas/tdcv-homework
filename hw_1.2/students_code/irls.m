function [cam_orientation, cam_location, loss_history, lambda_history] = irls( ...
    world_coords, expected_img_coords, camera_params, ...
    cam_orientation, cam_location, ...
    threshold_irls, N)

loss_history = NaN(N, 1);
lambda_history = NaN(N, 1);
lambda = 0.001;
u = threshold_irls + 1;
residuals = get_residuals(world_coords, expected_img_coords, camera_params, cam_orientation, cam_location);
for t=1:N
    if u <= threshold_irls
        fprintf('  Stopping IRLS at t=%d; u=%f\n', t, u);
        break
    end
    sigma = 1.48257968 * mad(residuals, 1);
    loss = reprojection_loss(residuals);
    loss_history(t) = loss;
    lambda_history(t) = lambda;
    W = get_weights(residuals, sigma);
    %J = eval_numeric_gradient(world_coords, expected_img_coords, camera_params, cam_orientation, cam_location);
    J = eval_jacobian(world_coords, camera_params, cam_orientation, cam_location);
    
    update = -inv(J'*W*J + eye(6).*lambda)* J'*W*residuals;
    
    [new_rotation_matrix, new_location] = parse_param_update(update, cam_orientation, cam_location);
    new_residuals = get_residuals(world_coords, expected_img_coords, camera_params, new_rotation_matrix, new_location);
    new_loss = reprojection_loss(new_residuals);
    if new_loss > loss
        lambda = lambda*10;
    else
        lambda = lambda/10;
        cam_orientation = new_rotation_matrix;
        cam_location = new_location;
        residuals = new_residuals;
    end
    u = sqrt(sum(update.^2));

end

loss_history(t) = loss;
lambda_history(t) = lambda;

end

function [rotation_matrix,location]=parse_param_update(p, cam_orientation, cam_location)
old_rotation_v = rotationMatrixToVector(cam_orientation);
rotation_v = p(1:3) + old_rotation_v';
location = p(4:6)' + cam_location;
rotation_matrix = rotationVectorToMatrix(rotation_v);
end