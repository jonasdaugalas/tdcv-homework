%% Check Tukey
hold on
x = linspace(-6, 6, 50);
plot(x, Tukey(x));
plot(x, x.^2);
ylim([0 12]);
hold off

%% 