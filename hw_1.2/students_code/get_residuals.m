function [res] = get_residuals(...
    world_coords, expected_img_coords, camera_params, cam_orientation, cam_location)

projected_img_coords = project3d2image(...
    world_coords', camera_params, cam_orientation, cam_location);
%res = reshape((projected_img_coords - expected_img_coords), [size(projected_img_coords, 2)*2 , 1]);
res = (projected_img_coords - expected_img_coords);
res = reshape(res, 1, [])';
end