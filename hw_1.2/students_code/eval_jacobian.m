function J = eval_jacobian(world_coords, camera_params, camera_orientation, camera_location)

J = NaN(size(world_coords, 1)*2, 6);
v = -rotationMatrixToVector(camera_orientation);
P = camera_params.IntrinsicMatrix' * [camera_orientation -camera_orientation*camera_location'];
homo_world_coords = [world_coords ones(size(world_coords,1), 1)];
projections_homo = (P* homo_world_coords')';
for i=1:size(world_coords, 1)
    % j = get_point_jacobian(camera_params, camera_orientation, v, world_coords(1,:)', projections_homo(i,:));
    j = get_point_jacobian(camera_params, camera_orientation, v, camera_location', world_coords(i,:)', projections_homo(i,:));
    J( ((i-1)*2)+1:i*2, :) = j;
end

end

function j=get_point_jacobian(camera_params, rot_matrix, v, camera_location, world_coord, img_homo_coord)
U = img_homo_coord(1);
V = img_homo_coord(2);
W = img_homo_coord(3);
dWorldToCam_dParams = [1/W, 0, -U/(W^2); 0, 1/W, -V/(W^2);];
dCamToHomo_dWorldToCam = camera_params.IntrinsicMatrix';

% dr1 = ((v(1)*skew_matrix(v) + skew_matrix(cross(v,((eye(3)- rot_matrix')*[1;0;0]))))./sum(v.^2)) * rot_matrix';
% dr2 = ((v(2)*skew_matrix(v) + skew_matrix(cross(v,((eye(3)- rot_matrix')*[0;1;0]))))./sum(v.^2)) * rot_matrix';
% dr3 = ((v(3)*skew_matrix(v) + skew_matrix(cross(v,((eye(3)- rot_matrix')*[0;0;1]))))./sum(v.^2)) * rot_matrix';

dr1 = rodrigues(rot_matrix, v, 1);
dr2 = rodrigues(rot_matrix, v, 2);
dr3 = rodrigues(rot_matrix, v, 3);

dHomoTo2D_dCamToHomo = [-dr1*world_coord+dr1*camera_location -dr2*world_coord+dr2*camera_location -dr3*world_coord+dr3*camera_location -rot_matrix];
j = dWorldToCam_dParams*dCamToHomo_dWorldToCam*dHomoTo2D_dCamToHomo;
end

% function j=get_point_jacobian(camera_params, rot_matrix, v, world_coord, img_homo_coord)
% U = img_homo_coord(1);
% V = img_homo_coord(2);
% W = img_homo_coord(3);
% dWorldToCam_dParams = [1/W, 0, -U/(W^2); 0, 1/W, -V/(W^2);];
% dCamToHomo_dWorldToCam = camera_params.IntrinsicMatrix';
% dHomoTo2D_dCamToHomo = [...
%     rodrigues(rot_matrix, v, 1)*world_coord...
%     rodrigues(rot_matrix, v, 2)*world_coord...
%     rodrigues(rot_matrix, v, 3)*world_coord...
%     eye(3)];
% j = dWorldToCam_dParams*dCamToHomo_dWorldToCam*dHomoTo2D_dCamToHomo;
% end

function res=rodrigues(rot_matrix, v, i)
std_basis = eye(3);
v_norm_sqr = sum(v.^2);
v_skew = skew_matrix(v);
term2 = skew_matrix(cross(v, (eye(3)-rot_matrix)*std_basis(:, i)));
res = (((v_skew.* v(i)) + term2) ./ v_norm_sqr) * rot_matrix;
end

function res = skew_matrix(v)
res = [0 -v(3) v(2); v(3) 0 -v(1); -v(2) v(1) 0];
end