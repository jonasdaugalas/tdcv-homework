function res=Tukey(residuals)
c = 4.685;
res  = (c^2/6) .* (1 - (1 - (residuals./c).^2).^3) ;
res(abs(residuals)>c) = (c^2)/6;
end