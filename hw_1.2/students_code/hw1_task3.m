clear
clc
close all
addpath('helper_functions')

%% Settings
VLFEAT_ROOT_PATH = '/opt/vlfeat'; % Installation root of VLFeat
LOAD_SIFT_DESCRIPTORS=true;
LOAD_FIRST_IMAGE_POSE=true;

%% Setup
% path to the images folder
path_img_dir = '../data/TDCV_tracking/validation/img';
% path to object ply file
object_path = '../data/teabox.ply';
% path to results folder
results_path = '../data/TDCV_tracking/validation/results';
% Setup VLFeat
run(fullfile(VLFEAT_ROOT_PATH, 'toolbox/vl_setup'));

% Read the object's geometry
% Here vertices correspond to object's corners and faces are triangles
[vertices, faces] = read_ply(object_path);
num_faces = size(faces, 1);

% Create directory for results
if ~exist(results_path,'dir')
    mkdir(results_path);
end

% Load Ground Truth camera poses for the validation sequence
% Camera orientations and locations in the world coordinate system
load('gt_valid.mat')

% Camera intrinsics
focal_length = [2960.37845, 2960.37845];
principal_point = [1841.68855, 1235.23369];
image_size = [3680, 2456];
camera_params = cameraIntrinsics(focal_length, principal_point, image_size);

%% Group coordinates per face (triangle)

% tri_coords: 12x3x3 (n_faces x vtx_per_face x coords_per_vtx)
tri_coords = NaN(num_faces, 3 ,3);
for i=1:num_faces
    face_corners = faces(i,:) + 1;
    tri_coords(i,:,:) = vertices(face_corners(:),:);
end
vtx_x = squeeze(tri_coords(:,1,:));
vtx_y = squeeze(tri_coords(:,2,:));
vtx_z = squeeze(tri_coords(:,3,:));

%% Get all filenames in images folder

FolderInfo = dir(fullfile(path_img_dir, '*.JPG'));
Filenames = fullfile(path_img_dir, {FolderInfo.name} );
num_files = length(Filenames);

%% Detect SIFT keypoints in all images

% You will need vl_sift() and vl_ubcmatch() functions
% download vlfeat (http://www.vlfeat.org/download.html) and unzip it somewhere
% Don't forget to add vlfeat folder to MATLAB path

% Place SIFT keypoints and corresponding descriptors for all images here
keypoints = cell(num_files,1);
descriptors = cell(num_files,1);

if LOAD_SIFT_DESCRIPTORS
    load('sift_descriptors.mat');
    load('sift_keypoints.mat');
else
    for i=1:num_files
        fprintf('Calculating sift features for image: %d \n', i);
        img = single(rgb2gray(imread(char(Filenames(i)))));
        [keypoints{i}, descriptors{i}] = vl_sift(img) ;
    end
    %save sift features and descriptors and load them when you rerun the code to save time
    save('sift_descriptors.mat', 'descriptors')
    save('sift_keypoints.mat', 'keypoints')
end

%% Initialization: Compute camera pose for the first image

% As the initialization step for tracking
% you need to compute the camera pose for the first image
% The first image and it's camera pose will be your initial frame
% and initial camera pose for the tracking process

% You can use estimateWorldCameraPose() function or your own implementation
% of the PnP+RANSAC from previous tasks

% You can get correspondences for PnP+RANSAC either using your SIFT model from the previous tasks
% or by manually annotating corners (e.g. with mark_images() function)

% Place predicted camera orientations and locations in the world coordinate system for all images here
cam_in_world_orientations = zeros(3,3,num_files);
cam_in_world_locations = zeros(1,3,num_files);

sift_matches = cell(num_files, 1);

if LOAD_FIRST_IMAGE_POSE
    load('first_image_orientation.mat');
    load('first_image_location.mat');
    load('first_image_sift_matches.mat');
else
    load('sift_model_hw1.1.mat');
    [orientation, location, matches] = get_single_cam_pose(...
        model, keypoints{1}, descriptors{1}, camera_params);
    save('first_image_orientation.mat', 'orientation');
    save('first_image_location.mat', 'location');
    save('first_image_sift_matches.mat', 'matches');
end

cam_in_world_orientations(:,:,1) = orientation;
cam_in_world_locations(:,:,1) = location;
sift_matches{1} = matches;
clearvars model orientation location matches

%Visualise the pose for the initial frame
edges = [
    [1, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 7]
    [2, 4, 5, 3, 6, 4, 7, 8, 6, 8, 7, 8]];
figure()
hold on;
imshow(char(Filenames(1)), 'InitialMagnification', 'fit');
title(sprintf('Initial Image Camera Pose'));
% Plot bounding box
points = project3d2image(vertices', camera_params, cam_in_world_orientations(:,:,1), cam_in_world_locations(:, :, 1));
for j=1:12
    plot(points(1, edges(:, j)), points(2, edges(:,j)), 'color', 'b');
end
hold off;

%% IRLS nonlinear optimisation

% Now you need to implement the method of iteratively reweighted least squares (IRLS)
% to optimise reprojection error between consecutive image frames

% Method steps:
% 1) Back-project SIFT keypoints from the initial frame (image i) to the object using the
% initial camera pose and the 3D ray intersection code from the task 1.
% This will give you 3D coordinates (in the world coordinate system) of the
% SIFT keypoints from the initial frame (image i) that correspond to the object
% 2) Find matches between descriptors of back-projected SIFT keypoints from the initial frame (image i) and the
% SIFT keypoints from the subsequent frame (image i+1) using vl_ubcmatch() from VLFeat library
% 3) Project back-projected SIFT keypoints onto the subsequent frame (image i+1) using 3D coordinates from the
% step 1 and the initial camera pose
% 4) Compute the reprojection error between 2D points of SIFT
% matches for the subsequent frame (image i+1) and 2D points of projected matches
% from step 3
% 5) Implement IRLS: for each IRLS iteration compute Jacobian of the reprojection error with respect to the pose
% parameters and update the camera pose for the subsequent frame (image i+1)
% 6) Now the subsequent frame (image i+1) becomes the initial frame for the
% next subsequent frame (image i+2) and the method continues until camera poses for all
% images are estimated

% We suggest you to validate the correctness of the Jacobian implementation
% either using Symbolic toolbox or finite differences approach

threshold_irls = 0.0001; % update threshold for IRLS
N = 40; % number of iterations
threshold_ubcmatch = 5; % matching threshold for vl_ubcmatch()

sift_models = cell(num_files, 1);

% Prepare first frame
sift_models{1} = build_sift_model(1,cam_in_world_orientations,cam_in_world_locations,...
    keypoints,descriptors,vtx_x,vtx_y,vtx_z,camera_params,num_faces);

DEBUG_MODE = true;

for i=2:num_files
    
    fprintf('Estimating camera pose: %d\n', i);
    % Match sifts between model of i-1 frame and the current frame
    sift_matches = vl_ubcmatch(descriptors{i}, sift_models{i-1}.descriptors, threshold_ubcmatch);
    matched_img_coords = keypoints{i}(1:2, sift_matches(1,:)); % 2xN
    matched_world_coords = sift_models{i-1}.coord3d(sift_matches(2,:),:); % Nx3
    
    % Run IRLS to get good camera pose
    [new_cam_orientation, new_cam_location, loss_history, lambda_history] = irls( ...
        matched_world_coords, matched_img_coords, camera_params, ...
        cam_in_world_orientations(:,:,i-1), cam_in_world_locations(:,:,i-1), ...
        threshold_irls, N);
    
    if DEBUG_MODE
        exit_debug = show_debug_figs(...
            i, N, char(Filenames(i)), keypoints{i}, sift_matches,...
            loss_history, lambda_history);
        DEBUG_MODE = ~exit_debug;
        close all
    end
    
    % Store good camera pose
    cam_in_world_orientations(:,:,i) = new_cam_orientation;
    cam_in_world_locations(:,:,i) = new_cam_location;
    
    % Build model for the current frame with the good camera pose
    sift_models{i} = build_sift_model(i,cam_in_world_orientations,cam_in_world_locations,...
        keypoints,descriptors,vtx_x,vtx_y,vtx_z,camera_params,num_faces);
    
    
end

%% Plot camera trajectory in 3D world CS + cameras

figure()
% Predicted trajectory
visualise_trajectory(vertices, edges, cam_in_world_orientations, cam_in_world_locations, 'Color', 'b');
hold on;
% Ground Truth trajectory
visualise_trajectory(vertices, edges, gt_valid.orientations, gt_valid.locations, 'Color', 'g');
hold off;
title('\color{green}Ground Truth trajectory \color{blue}Predicted trajectory')
savefig('validation_dataset_trajectory.fig');

%% Visualize bounding boxes

figure()
for i=1:num_files
    
    imshow(char(Filenames(i)), 'InitialMagnification', 'fit');
    set(gcf, 'units','normalized','outerposition',[0.1 0.1 0.8 0.8]);
    title(sprintf('Image: %d', i))
    hold on
    % Ground Truth Bounding Boxes
    points_gt = project3d2image(vertices',camera_params, gt_valid.orientations(:,:,i), gt_valid.locations(:, :, i));
    % Predicted Bounding Boxes
    points_pred = project3d2image(vertices',camera_params, cam_in_world_orientations(:,:,i), cam_in_world_locations(:, :, i));
    for j=1:12
        plot(points_gt(1, edges(:, j)), points_gt(2, edges(:,j)), 'color', 'g');
        plot(points_pred(1, edges(:, j)), points_pred(2, edges(:,j)), 'color', 'b');
    end
    hold off;
    
    filename = fullfile(results_path, strcat('image', num2str(i), '.png'));
    saveas(gcf, filename)
end

%% TEST dataset. Init

path_img_dir = '../data/TDCV_tracking/test/img';
results_path = '../data/TDCV_tracking/test/results';
FolderInfo = dir(fullfile(path_img_dir, '*.JPG'));
Filenames = fullfile(path_img_dir, {FolderInfo.name} );
num_files = length(Filenames);

% Create directory for results
if ~exist(results_path,'dir')
    mkdir(results_path);
end

%% TEST dataset. Detect SIFT keypoints in all images
keypoints = cell(num_files,1);
descriptors = cell(num_files,1);

if LOAD_SIFT_DESCRIPTORS
    load('test_sift_descriptors.mat');
    load('test_sift_keypoints.mat');
else
    for i=1:num_files
        fprintf('Calculating sift features for image: %d \n', i);
        img = single(rgb2gray(imread(char(Filenames(i)))));
        [keypoints{i}, descriptors{i}] = vl_sift(img) ;
    end
    %save sift features and descriptors and load them when you rerun the code to save time
    save('test_sift_descriptors.mat', 'descriptors')
    save('test_sift_keypoints.mat', 'keypoints')
end

%% TEST dataset. Camera pose for the first image

cam_in_world_orientations = zeros(3,3,num_files);
cam_in_world_locations = zeros(1,3,num_files);

if LOAD_FIRST_IMAGE_POSE
    load('test_first_image_orientation.mat');
    load('test_first_image_location.mat');
else
    labeled_points = mark_image(char(Filenames(1)), 8);
    [visible_image_points, removed] = rmmissing(labeled_points);
    visible_vertices = vertices(~removed,:);
    max_reproj_err = 3;
    
    fprintf('Estimating pose for the 1st test dataset image\n');
    [orientation, location] = estimateWorldCameraPose(...
        visible_image_points, visible_vertices, camera_params, 'MaxReprojectionError', max_reproj_err);
    
    save('test_first_image_orientation.mat', 'orientation');
    save('test_first_image_location.mat', 'location');
end

cam_in_world_orientations(:,:,1) = orientation;
cam_in_world_locations(:,:,1) = location;
clearvars orientation location

% Visualize first pose
edges = [
    [1, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 7]
    [2, 4, 5, 3, 6, 4, 7, 8, 6, 8, 7, 8]];
figure()
hold on;
imshow(char(Filenames(1)), 'InitialMagnification', 'fit');
title(sprintf('Initial Image Camera Pose'));
% Plot bounding box
points = project3d2image(vertices', camera_params, cam_in_world_orientations(:,:,1), cam_in_world_locations(:, :, 1));
for j=1:12
    plot(points(1, edges(:, j)), points(2, edges(:,j)), 'color', 'b');
end
hold off;

%% TEST dataset. IRLS nonlin sequential cam pose estimation

threshold_irls = 0.0001; % update threshold for IRLS
N = 40; % number of iterations
threshold_ubcmatch = 5; % matching threshold for vl_ubcmatch()

sift_models = cell(num_files, 1);

% Prepare first frame
sift_models{1} = build_sift_model(1,cam_in_world_orientations,cam_in_world_locations,...
    keypoints,descriptors,vtx_x,vtx_y,vtx_z,camera_params,num_faces);

DEBUG_MODE = true;

for i=2:num_files
    
    fprintf('Estimating camera pose: %d\n', i);
    % Match sifts between model of i-1 frame and the current frame
    sift_matches = vl_ubcmatch(descriptors{i}, sift_models{i-1}.descriptors, threshold_ubcmatch);
    matched_img_coords = keypoints{i}(1:2, sift_matches(1,:)); % 2xN
    matched_world_coords = sift_models{i-1}.coord3d(sift_matches(2,:),:); % Nx3
    
    
    % Run IRLS to get good camera pose
    [new_cam_orientation, new_cam_location, loss_history, lambda_history] = irls( ...
        matched_world_coords, matched_img_coords, camera_params, ...
        cam_in_world_orientations(:,:,i-1), cam_in_world_locations(:,:,i-1), ...
        threshold_irls, N);
    
    if DEBUG_MODE
        exit_debug = show_debug_figs(...
            i, N, char(Filenames(i)), keypoints{i}, sift_matches,...
            loss_history, lambda_history);
        DEBUG_MODE = ~exit_debug;
        close all
    end
    
    % Store good camera pose
    cam_in_world_orientations(:,:,i) = new_cam_orientation;
    cam_in_world_locations(:,:,i) = new_cam_location;
    
    % Build model for the current frame with the good camera pose
    sift_models{i} = build_sift_model(i,cam_in_world_orientations,cam_in_world_locations,...
        keypoints,descriptors,vtx_x,vtx_y,vtx_z,camera_params,num_faces);
    
    
end

%% TEST dataset. Plot camera trajectory in 3D world CS + cameras

figure()
% Predicted trajectory
visualise_trajectory(vertices, edges, cam_in_world_orientations, cam_in_world_locations, 'Color', 'b');
title('\color{green}Ground Truth trajectory \color{blue}Predicted trajectory');
savefig('test_dataset_trajectory.fig');

%% TEST dataset. Visualize bounding boxes

figure()
for i=1:num_files
    
    imshow(char(Filenames(i)), 'InitialMagnification', 'fit');
    set(gcf, 'units','normalized','outerposition',[0.1 0.1 0.8 0.8]);
    title(sprintf('Image: %d', i))
    hold on
    % Predicted Bounding Boxes
    points_pred = project3d2image(vertices',camera_params, cam_in_world_orientations(:,:,i), cam_in_world_locations(:, :, i));
    for j=1:12
        plot(points_pred(1, edges(:, j)), points_pred(2, edges(:,j)), 'color', 'b');
    end
    hold off;
    
    filename = fullfile(results_path, strcat('image', num2str(i), '.png'));
    saveas(gcf, filename)
end
%% Bonus part

% Save estimated camera poses for the validation sequence using Vision TUM trajectory file
% format: https://vision.in.tum.de/data/datasets/rgbd-dataset/file_formats
% Then estimate Absolute Trajectory Error (ATE) and Relative Pose Error for
% the validation sequence using python tools from: https://vision.in.tum.de/data/datasets/rgbd-dataset/tools
% In this task you should implement you own function to convert rotation matrix to quaternion

% Save estimated camera poses for the test sequence using Vision TUM
% trajectory file format

% Attach the file with estimated camera poses for the test sequence to your code submission
% If your code and results are good you will get a bonus for this exercise
% We are expecting the mean absolute translational error (from ATE) to be
% approximately less than 1cm

% TODO: Estimate ATE and RPE for validation and test sequences
