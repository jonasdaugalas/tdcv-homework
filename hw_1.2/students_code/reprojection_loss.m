function [loss] = reprojection_loss(residuals)
loss = sum(Tukey(residuals));
end
