function [debug_off] = show_debug_figs(i, num_iterations, img_path,keypoints, sift_matches, loss_history, lambda_history)
figure()
hold on;
set(gcf, 'units', 'Normalized', 'OuterPosition', [0.5, 0.5, 0.5, 0.5]);
imshow(img_path, 'InitialMagnification', 'fit');
vl_plotframe(keypoints(:, sift_matches(1,:)), 'linewidth',2);
title(sprintf('SIFT matches between %d and %d frames', i, i-1));
hold off;
figure('Name', sprintf('Loss history: %d', i));
plot(1:num_iterations, loss_history);
set(gcf, 'units', 'Normalized', 'OuterPosition', [0.0, 0.0, 0.5, 0.5]);
figure('Name', sprintf('Lambda history: %d', i));
plot(1:num_iterations, lambda_history);
set(gca, 'YScale', 'log');
set(gcf, 'units', 'Normalized', 'OuterPosition', [0.5, 0.0, 0.5, 0.5]);
choice = menu('You are in debug mode','Continue','Exit debug mode');
if choice == 2
    debug_off = true;
else
    debug_off = false;
end
end

