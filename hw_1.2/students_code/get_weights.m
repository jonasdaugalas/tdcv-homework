function [W] = get_weights(residuals, sigma)
c = 4.685;
ei=residuals./sigma;
W = diag( ((1 - (ei.^2 ./ c^2)).^2) .* (abs(ei) < c));
end