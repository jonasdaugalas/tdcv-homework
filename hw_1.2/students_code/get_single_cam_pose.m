function [cam_orientations, cam_locations, sift_matches] = get_single_cam_pose(...
    model, new_keypoints, new_descriptors, camera_params)

fprintf('[get_single_cam_pose] Start matching\n');
threshold_ubcmatch = 2;
% Match features between SIFT model and SIFT features from new image
sift_matches = vl_ubcmatch(new_descriptors, model.descriptors, threshold_ubcmatch);
fprintf('[get_single_cam_pose] Num sift matches: %d\n', size(sift_matches, 2));

[cam_orientations, cam_locations,]=ransac(...
    sift_matches, model, new_keypoints, camera_params);

end

