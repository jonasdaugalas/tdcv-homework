import os
import re
import itertools

import numpy as np
import torch
from torch.utils.data import Dataset
import matplotlib.pyplot as plt

from labeledimage import LabeledImage


class HW3Dataset:

    ALL_CLASSES = ['ape', 'benchvise', 'cam', 'cat', 'duck']
    IMG_ID_REGEXP = re.compile(r'.*?([0-9]+).png')

    @staticmethod
    def parse_image_id_from_name(img_name):
        img_id_match = HW3Dataset.IMG_ID_REGEXP.match(img_name)
        if img_id_match is None:
            raise RuntimeError('Could not parse image id', img_name)
        return int(img_id_match.group(1))

    @staticmethod
    def merge(a, b):
        result = HW3Dataset()
        result.labeled_images = a.labeled_images + b.labeled_images
        return result

    def __init__(self, path=None):
        self.labeled_images = []
        if path is not None:
            self._initialize_from_directory(path)

    def __len__(self):
        return len(self.labeled_images)

    def __getitem__(self, idx):
        return self.labeled_images[idx]

    def _initialize_from_directory(self, path):
        for c in HW3Dataset.ALL_CLASSES:
            class_path = os.path.join(path, c)
            listing = os.listdir(class_path)
            img_names = [fname for fname in listing if fname.endswith('.png')]
            poses_fpath = os.path.join(class_path, 'poses.txt')
            quaternions = self._parse_quaternions_file(poses_fpath)
            for img_file_name in img_names:
                img_id = self.parse_image_id_from_name(img_file_name)
                img_path = os.path.join(class_path, img_file_name)
                self.labeled_images.append(
                    LabeledImage(
                        id=img_id,
                        path=img_path,
                        label=c,
                        pose=quaternions[img_id]))

    def _parse_quaternions_file(self, path):
        with open(path, 'r') as f:
            lines = f.readlines()

        quaternions = {}
        for fname, quaternion in zip(lines[::2], lines[1::2]):
            img_id = self.parse_image_id_from_name(fname)
            quaternion_coords = [float(coord) for coord in quaternion.split()]
            quaternions[img_id] = quaternion_coords

        return quaternions

    def train_split(self, train_split_fpath):
        # Needs not fucked up ids!!!
        with open(train_split_fpath, 'r') as f:
            contents = f.read()
            train_indices = [int(idx) for idx in contents.split(', ')]

        train_dataset = HW3Dataset()
        test_dataset = HW3Dataset()
        for img in self.labeled_images:
            if img.id in train_indices:
                train_dataset.labeled_images.append(img)
            else:
                test_dataset.labeled_images.append(img)

        return train_dataset, test_dataset


class TripletDataset(Dataset):

    def __init__(self, dataset, db, max_pushers=1000):
        self.dataset = dataset
        self.db = db
        self.max_pushers = max_pushers
        self.pullers = self._find_pullers()
        self.pushers, self.pusher_to_puller_index = self._find_pushers()

    def _find_pullers(self):
        pullers = [None] * len(self.dataset)
        for i, anchor in enumerate(self.dataset):
            best_pose_diff = float('inf')
            for candidate in self.db:
                if anchor.label != candidate.label:
                    continue
                pose_diff = anchor.get_pose_difference(candidate)
                if pose_diff < best_pose_diff:
                    best_pose_diff = pose_diff
                    pullers[i] = candidate

            if pullers[i] is None:
                raise RuntimeError('Could not find puller', anchor.id)
        return pullers


    def _find_pushers(self):
        pushers = []
        pusher_to_puller_index = []
        for i, puller in enumerate(self.pullers):
            num_current_pushers = 0
            for candidate in self.db:
                if puller.label == candidate.label and puller.is_similar(candidate):
                    continue
                pushers.append(candidate)
                pusher_to_puller_index.append(i)
                num_current_pushers += 1
                if num_current_pushers >= self.max_pushers:
                    break
        return pushers, pusher_to_puller_index

    def __len__(self):
        return len(self.pushers)

    def __getitem__(self, idx):
        anchor = self.dataset[self.pusher_to_puller_index[idx]]
        puller = self.pullers[self.pusher_to_puller_index[idx]]
        pusher = self.pushers[idx]
        return torch.Tensor([
            anchor.get_img_data(),
            puller.get_img_data(),
            pusher.get_img_data()])
