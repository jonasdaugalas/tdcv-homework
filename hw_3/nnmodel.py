import torch
import torch.nn as nn
import torchvision.models as models
import torch.nn.functional as F

class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

class NeuralNet(nn.Module):

    def __init__(self):
        super(NeuralNet, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=(8, 8), stride=1)
        self.conv2 = nn.Conv2d(in_channels=16, out_channels=7, kernel_size=(5, 5), stride=1)
        self.maxpool = nn.MaxPool2d(2, 2)
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(7*12*12, 265)
        self.fc2 = nn.Linear(265, 16)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(self.maxpool(x))
        x = self.conv2(x)
        x = F.relu(self.maxpool(x))
        x = self.flatten(x)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x
