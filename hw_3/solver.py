import numpy as np
import torch


class Solver(object):
    default_adam_args = {"lr": 1e-4,
                         "betas": (0.9, 0.999),
                         "eps": 1e-8,
                         "weight_decay": 0.0}

    def __init__(self, optim=torch.optim.Adam, optim_args={},
                 loss_func=torch.nn.MSELoss()):
        optim_args_merged = self.default_adam_args.copy()
        optim_args_merged.update(optim_args)
        self.optim_args = optim_args_merged
        self.optim = optim
        self.loss_func = loss_func

        self._reset_histories()

    def _reset_histories(self):

        self.train_loss_history = []
        self.train_acc_history = []
        self.val_acc_history = []
        self.val_loss_history = []

    def train(self, model, train_loader, num_epochs=10, log_nth=0):

        optim = self.optim(model.parameters(), **self.optim_args)
        self._reset_histories()
        iter_per_epoch = len(train_loader)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        model.to(device)

        print('START TRAIN.')
        model.train()
        for epoch in range(num_epochs):
            for batch_idx, batch in enumerate(train_loader):

                anchor_set = torch.FloatTensor(batch.size(0), 3, 64, 64)
                puller_set = torch.FloatTensor(batch.size(0), 3, 64, 64)
                pusher_set = torch.FloatTensor(batch.size(0), 3, 64, 64)

                for i,sample in enumerate(batch):
                    anchor_set[i,:,:,:] = sample[0].permute(2, 0, 1)
                    puller_set[i,:,:,:] = sample[1].permute(2, 0, 1)
                    pusher_set[i,:,:,:] = sample[2].permute(2, 0, 1)

                anchor_set = anchor_set.to(device)
                puller_set = puller_set.to(device)
                pusher_set = pusher_set.to(device)

                # zero the parameter gradients
                optim.zero_grad()

                # forward pass for batch
                anchor_outputs = model(anchor_set)
                puller_outputs = model(puller_set)
                pusher_outputs = model(pusher_set)

                # to the loss function you pass the outputs for each of the images from the triplet

                # get loss compared to processed ground truth, store loss
                loss = self.loss_func(anchor_outputs, puller_outputs, pusher_outputs)
                # self.train_loss_history.append(loss.item())
                self.train_loss_history.append(loss.item())

                # backward pass
                loss.backward()

                # adjust new weights via optimiser
                optim.step()

                if batch_idx % 100 == 0:
                    print(loss.item())
            print('epoch {} finished'.format(epoch + 1))
        print('FINISH.')
