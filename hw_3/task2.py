import os
import pickle
import argparse
import random
import time
from torch.utils.data import DataLoader
import nnmodel
import loss
import solver as nnsolver


def main():

    argparser = argparse.ArgumentParser()
    argparser.add_argument('path', help='Path to pickled datasets.')
    args = argparser.parse_args()

    datasets = pickle.load(open(args.path, 'rb'))

    print('Training dataset has {} triplets'.format(len(datasets['triplets'])))
    dataloader = DataLoader(datasets['triplets'], batch_size=1, shuffle=True)


    model = nnmodel.NeuralNet()
    loss_func = loss.TripletPairLoss()
    solver = nnsolver.Solver(loss_func=loss_func)

    solver.train(model, dataloader, num_epochs=2)


if __name__ == '__main__':
    main()
