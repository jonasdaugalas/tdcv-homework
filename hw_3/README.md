# How to run

Activate your environments (with pytorch, matplotlib, etc.) if needed.

Parse data:
```
python task1.py parse path/to/data/directory
```

Parse data and write out to pickle :
```
python task1.py parse path/to/data/directory --write-pickle path/to/pickle_file.p
```

Parse data, limit number of pushers:
```
python task1.py parse path/to/data/directory --max-pushers 42
```

Load data from pickled datasets file:
```
python task1.py load path/to/pickle_file.p
```

Train neural network:
```
python task2.py path/to/pickle_file.p
```
