import torch
import torch.nn as nn


class TripletPairLoss(torch.nn.Module):

    def __init__(self):
        super(TripletPairLoss, self).__init__()

    def forward(self, anchor_outputs, puller_outputs, pusher_outputs):
        return self.triplet_loss(anchor_outputs, puller_outputs, pusher_outputs) + self.pair_loss(anchor_outputs, puller_outputs)

    def triplet_loss(self, anchor_outputs, puller_outputs, pusher_outputs):
        loss_sum = torch.tensor(0, dtype=torch.float)
        diff_pos = anchor_outputs - puller_outputs  # f(Xa) - f(X+)
        diff_neg = anchor_outputs - pusher_outputs  # f(Xa) - f(X-)

        for diff_pos_i, diff_neg_i in zip(diff_pos, diff_neg):
            curr_loss = ((torch.clamp(1 - (
                torch.sum(torch.pow(diff_neg_i, 2)) / (
                torch.sum(torch.pow(diff_pos_i, 2)) + 0.01)), min=0)))
            loss_sum += curr_loss
        return loss_sum

    def pair_loss(self, anchor_outputs, puller_outputs):
        loss_sum = torch.tensor(0, dtype=torch.float)
        diff_pos = anchor_outputs - puller_outputs  # f(Xa) - f(X+)
        for diff_pos_i in diff_pos:
            loss_sum += torch.sum(torch.pow((diff_pos_i), 2))
        return loss_sum
