import os
import pickle
import argparse
import random
import time
import torch
from torch.utils.data import DataLoader
import numpy as np
import matplotlib.pyplot as plt
import dataset


def prepare_data(datapath, max_pushers):
    ds_path = os.path.join(datapath, 'dataset')

    datasets = {}
    ds_real = dataset.HW3Dataset(os.path.join(ds_path, 'real'))
    ds_fine = dataset.HW3Dataset(os.path.join(ds_path, 'fine'))
    datasets['db'] = dataset.HW3Dataset(os.path.join(ds_path, 'coarse'))

    ds_real_train, ds_real_test = ds_real.train_split(
        os.path.join(ds_path, 'real', 'training_split.txt'))

    datasets['train'] = dataset.HW3Dataset.merge(ds_real_train, ds_fine)
    datasets['test'] = ds_real_test
    datasets['triplets'] = dataset.TripletDataset(
        datasets['train'], datasets['db'], max_pushers=max_pushers)
    return datasets


def main():

    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        'mode',
        help=('Mode:\n'
              '\t"parse" - parse data from directory;'
              '\t"load" - load datasets from a pickle file'))
    argparser.add_argument(
        'path', help='Source')
    argparser.add_argument(
        '--max-pushers', type=int, default=1000,
        help='Number of max pushers')
    argparser.add_argument(
        '--write-pickle', required=False,
        help='Destination to write pickled datasets.')

    args = argparser.parse_args()

    if args.mode == 'load':
        datasets = pickle.load(
            open(args.path, 'rb'))
    else:
        datasets = prepare_data(args.path, args.max_pushers)
        if args.write_pickle is not None:
            pickle.dump(datasets, open(args.write_pickle, 'wb'))

    print('Training dataset has {} triplets'.format(len(datasets['triplets'])))
    dataloader = DataLoader(datasets['triplets'], batch_size=4, shuffle=True)
    for batch in dataloader:
        for anchor, pull, push in batch:
            figure, axes = plt.subplots(1, 3)
            axes[0].imshow(anchor)
            axes[1].imshow(pull)
            axes[2].imshow(push)
            plt.show()


if __name__ == '__main__':
    main()
