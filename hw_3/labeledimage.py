import matplotlib.pyplot as plt
import numpy as np


class LabeledImage():

    POSE_SIMILARITY_THRESHOLD = 0.95

    def __init__(self, id, path, label, pose):
        self.id = id
        self.path = path
        self.label = label
        self.pose = np.array(pose)
        # Lazily loaded image data
        self._img_data = None

    def get_img_data(self):
        if self._img_data is None:
            # Read image if not loaded yet
            self._img_data = plt.imread(self.path)
        return self._img_data

    def get_pose_difference(self, other):
        return 2 * np.arccos(min(1, abs(self.pose @ other.pose)))

    def is_similar(self, other):
        if self.label != other.label:
            return False
        # Quaternion dot product is 1 or -1 when orientation is equal
        pose_similarity = abs(self.pose @ other.pose)
        return pose_similarity >= LabeledImage.POSE_SIMILARITY_THRESHOLD
