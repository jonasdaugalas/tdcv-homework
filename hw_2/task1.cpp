#include <opencv2/core.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <stdio.h>

#include "lib/HOGDescriptor.h"
#include "lib/utils.h"

int main(int argc, char **argv) {

  cv::Mat img = cv::imread(getDataFilePath("task1/obj1000.jpg"), 1);
  if (!img.data) {
    printf("No image data \n");
    return -1;
  }

  cv::resize(img, img, cv::Size(COLS, ROWS));
  HOGDescriptor hog_obj = createDefaultHOGDescriptor();
  std::vector<cv::Mat> augmented = augmentImage(img);
  std::vector<std::vector<float>> features;
  for (auto image = augmented.begin(); image != augmented.end(); ++image) {
    std::vector<float> feats;
    hog_obj.detectHOGDescriptor(*image, feats, false);
    features.push_back(feats);
    hog_obj.visualizeHOG(*image, feats, 3);
  }

  return 0;
}
