#ifndef TDCW_HW2_UTILS_H
#define TDCW_HW2_UTILS_H

#include "HOGDescriptor.h"
#include <opencv2/core/mat.hpp>

#define COLS 128
#define ROWS 128
#define DATA_PATH "data/data/"

HOGDescriptor createDefaultHOGDescriptor();
std::vector<cv::Mat> augmentImage(cv::Mat img);
cv::String getDataFilePath(const char *subpath);
cv::Ptr<cv::ml::TrainData> prepareData(bool train, const char *path);
std::vector<cv::Rect> generateBoundingBoxes(cv::Size imgSize,
                                            cv::Size initialSize, float growth,
                                            int numSizes,
                                            float relativeStride = 0.25);
#endif
