#include "HOGDescriptor.h"

#include "tdcv-helpers/hog_visualization.cpp"

void HOGDescriptor::initDetector() {
  hog_detector = cv::HOGDescriptor(getWinSize(), getBlockSize(), getBlockStep(),
                                   getCellSize(), getNbins());
  is_init = true;
}

void HOGDescriptor::visualizeHOG(cv::Mat img, std::vector<float> &feat,
                                 int scale_factor) {
  if (!is_init) {
    initDetector();
  }
  ::visualizeHOG(img, feat, hog_detector, scale_factor);
}

void HOGDescriptor::detectHOGDescriptor(cv::Mat im, std::vector<float> &feat,
                                        bool show) {
  if (!is_init) {
    initDetector();
  }
  cv::Mat scaled;
  cv::resize(im, scaled, win_size);
  hog_detector.cv::HOGDescriptor::compute(scaled, feat);
  if (show) {
    HOGDescriptor::visualizeHOG(scaled, feat, 3);
  }
}

cv::HOGDescriptor &HOGDescriptor::getHog_detector() { return hog_detector; }

size_t HOGDescriptor::getFeatureSize() {
  return (size_t)nbins * (block_size.width / cell_size.width) *
         (block_size.height / cell_size.height) *
         ((win_size.width - block_size.width) / block_step.width + 1) *
         ((win_size.height - block_size.height) / block_step.height + 1);
}
