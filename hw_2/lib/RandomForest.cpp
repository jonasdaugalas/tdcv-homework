#include <algorithm>
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/core/hal/interface.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/types.hpp>
#include <random>
#include <string>
#include <utility>

#include "RandomForest.h"

RandomForest::RandomForest(int treeCount, int maxDepth, int CVFolds,
                           int minSampleCount, int maxCategories)
    : mTreeCount(treeCount), mMaxDepth(maxDepth), mCVFolds(CVFolds),
      mMinSampleCount(minSampleCount), mMaxCategories(maxCategories) {
  /*
    construct a forest with given number of trees and initialize all the trees
    with the given parameters
  */
  for (int i = 0; i < treeCount; ++i) {
    cv::Ptr<cv::ml::DTrees> tree = cv::ml::DTrees::create();
    tree->setCVFolds(CVFolds);
    tree->setMaxCategories(maxCategories);
    tree->setMaxDepth(maxDepth);
    tree->setMinSampleCount(minSampleCount);
    mTrees.push_back(tree);
  }
}

RandomForest::~RandomForest() {}

void RandomForest::save(const cv::String &path) {
  std::filesystem::create_directory(path);
  auto cfgOutPath = path / std::filesystem::path("cfg.txt");
  std::ofstream cfgOutFile(cfgOutPath);
  cfgOutFile << mMaxCategories;
  int counter = 0;
  for (auto tree : mTrees) {
    tree->save(
        (std::filesystem::path(path) / "tree").concat(std::to_string(counter)));
    counter++;
  }
}

RandomForest *RandomForest::load(const cv::String &path) {
  auto cfgInPath = path / std::filesystem::path("cfg.txt");
  std::ifstream cfgInFile(cfgInPath);
  RandomForest *forest = new RandomForest();
  cfgInFile >> forest->mMaxCategories;
  forest->mTreeCount = 0;
  forest->mTrees.clear();
  for (const auto &treePath : std::filesystem::directory_iterator(path)) {
    if (treePath.path().filename().string() == "cfg.txt") {
      continue;
    }
    auto tree = cv::ml::DTrees::load(treePath.path());
    forest->mTrees.push_back(tree);
    forest->mTreeCount++;
  }
  return forest;
}

void RandomForest::train(cv::Ptr<cv::ml::TrainData> data, bool verbose) {

  for (auto i = 0; i < mTreeCount; ++i) {
    if (verbose) {
      std::cout << "Randomizing data for tree: " << i << "/" << mTreeCount
                << std::endl;
    }
    auto randomizedData = RandomForest::randomizeTreeTrainData(data);
    if (verbose) {
      std::cout << "Training...\n";
    }
    mTrees[i]->train(randomizedData);
  }
}

std::pair<float, float> RandomForest::predict(cv::InputArray samples,
                                              cv::OutputArray results) {
  cv::Mat predictions(samples.rows(), mTreeCount, CV_32F);
  cv::Mat newResults(samples.rows(), 2, CV_32F);
  for (auto i = 0; i < mTreeCount; ++i) {
    cv::Mat treePredictions;
    mTrees.at(i)->predict(samples, treePredictions);
    treePredictions.col(0).copyTo(predictions.col(i));
  }

  std::vector<int> classVotes(mMaxCategories);
  for (auto i = 0; i < predictions.rows; ++i) {
    std::fill(classVotes.begin(), classVotes.end(), 0);
    for (auto j = 0; j < mTreeCount; ++j) {
      int vote = round(predictions.at<float>(i, j));
      classVotes[vote] += 1;
    }
    auto max = std::max_element(classVotes.begin(), classVotes.end());
    int argmax = std::distance(classVotes.begin(), max);
    newResults.at<float>(i, 0) = float(argmax);
    newResults.at<float>(i, 1) = float(*max) / float(mTreeCount);
  }
  if (results.needed()) {
    results.clear();
    results.assign(newResults);
  }
  return std::pair(newResults.at<float>(0, 0), newResults.at<float>(0, 1));
}

cv::Ptr<cv::ml::TrainData>
RandomForest::randomizeTreeTrainData(cv::Ptr<cv::ml::TrainData> data) {

  cv::Mat samples = data->getSamples();
  cv::Mat labels = data->getResponses();
  int nSamples = samples.rows;
  int nFeatures = samples.cols;
  cv::Mat newSamples(0, nFeatures, CV_32F);
  cv::Mat newLabels(0, 1, CV_32S);

  std::random_device rd;  // obtain a random number from hardware
  std::mt19937 gen(rd()); // seed the generator
  std::uniform_int_distribution<int> distribution(0, nSamples - 1);

  for (auto i = 0; i < nSamples; ++i) {
    int rndIdx = distribution(gen);
    newSamples.push_back(samples.row(rndIdx));
    newLabels.push_back(labels.row(rndIdx));
  }

  return cv::ml::TrainData::create(newSamples, cv::ml::ROW_SAMPLE, newLabels);
}
