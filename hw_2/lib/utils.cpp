#include <cstring>
#include <filesystem>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/core/base.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/core/hal/interface.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <sys/cdefs.h>

#include "utils.h"

cv::String getDataFilePath(const char *subpath) {
  return cv::String(DATA_PATH) + cv::String(subpath);
}

HOGDescriptor createDefaultHOGDescriptor() {
  HOGDescriptor hog_obj;
  hog_obj.setWinSize(cv::Size(COLS, ROWS));
  hog_obj.setBlockSize(cv::Size((COLS / 8) * 2, (ROWS / 8) * 2));
  hog_obj.setBlockStep(cv::Size((COLS / 8), (ROWS / 8)));
  hog_obj.setCellSize(cv::Size((COLS / 8), (ROWS / 8)));
  hog_obj.setNbins(9);
  return hog_obj;
}

std::vector<cv::Mat> augmentImage(cv::Mat img) {
  std::vector<cv::Mat> augmented;
  cv::Mat grayscale, rot180, rot30, rot60, rot90, flipped,
      padded, skewed1, skewed2, skewed3, skewed4;

  cv::cvtColor(img, grayscale, cv::COLOR_BGR2GRAY);
  cv::rotate(img, rot180, cv::RotateFlags::ROTATE_180);
  cv::Point center(img.cols / 2, img.rows / 2);
  auto R = cv::getRotationMatrix2D(center, 30, 1);
  cv::warpAffine(img, rot30, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);
  R = cv::getRotationMatrix2D(center, 60, 1);
  cv::warpAffine(img, rot60, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);
  R = cv::getRotationMatrix2D(center, 90, 1);
  cv::warpAffine(img, rot90, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_CONSTANT);
  cv::copyMakeBorder(img, padded, 10, 0, 20, 0, cv::BORDER_REFLECT);
  cv::flip(img, flipped, 1);

  float skew1[6] = {1, 0.26, 0, 0, 1, 0};
  R = cv::Mat(2, 3, CV_32F, skew1);
  cv::warpAffine(img, skewed1, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);
  float skew2[6] = {1, 0, 0, 0.26, 1, 0};
  R = cv::Mat(2, 3, CV_32F, skew2);
  cv::warpAffine(img, skewed2, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);
  float skew3[6] = {1, -0.4, 0, 0, 1, 0};
  R = cv::Mat(2, 3, CV_32F, skew3);
  cv::warpAffine(img, skewed3, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);
  float skew4[6] = {1, 0, 0, -0.4, 1, 0};
  R = cv::Mat(2, 3, CV_32F, skew4);
  cv::warpAffine(img, skewed4, R, img.size(), cv::INTER_LINEAR,
                 cv::BORDER_REFLECT);

  augmented.push_back(img);
  augmented.push_back(grayscale);
  augmented.push_back(rot180);
  augmented.push_back(rot30);
  augmented.push_back(rot60);
  augmented.push_back(rot90);
  augmented.push_back(skewed1);
  augmented.push_back(skewed2);
  augmented.push_back(skewed3);
  augmented.push_back(skewed4);
  augmented.push_back(padded);
  augmented.push_back(flipped);

  return augmented;
}

cv::Ptr<cv::ml::TrainData> prepareData(bool train, const char *path) {
  HOGDescriptor hog = createDefaultHOGDescriptor();
  cv::Mat samples(0, hog.getFeatureSize(), CV_32F);
  cv::Mat labels(0, 1, CV_32S);
  cv::Mat imgOrig;
  cv::String directory = getDataFilePath(path) + (train ? "/train" : "/test");

  for (const auto &entry : std::filesystem::directory_iterator(directory)) {
    auto path = entry.path();
    int label = std::stoi(path.filename());
    for (const auto &imgPath : std::filesystem::directory_iterator(path)) {
      imgOrig = cv::imread(imgPath.path(), cv::IMREAD_COLOR);
      std::vector<cv::Mat> augmented;
      if (train) {
        augmented = augmentImage(imgOrig);
      } else {
        // No augmentation needed in TEST mode
        augmented.push_back(imgOrig);
      }
      for (const auto &img : augmented) {
        std::vector<float> feats;
        hog.detectHOGDescriptor(img, feats, false);
        cv::vconcat(samples, feats, samples);
        labels.push_back(float(label));
      }
    }
  }
  return cv::ml::TrainData::create(samples, cv::ml::ROW_SAMPLE, labels);
}

std::vector<cv::Rect> generateBoundingBoxes(cv::Size imgSize,
                                            cv::Size initialSize, float growth,
                                            int numSizes,
                                            float relativeStride) {

  std::vector<cv::Rect> bBoxes;
  for (int sizing = 0; sizing < numSizes; ++sizing) {
    int height = floor(initialSize.height * (std::pow(growth, sizing)));
    int width = floor(initialSize.width * (std::pow(growth, sizing)));
    int vStride = floor(height * relativeStride);
    int hStride = floor(width * relativeStride);
    int numVBoxes = ceil(imgSize.height / vStride);
    int numHBoxes = ceil(imgSize.width / hStride);
    if (imgSize.height - (numVBoxes - 1) * (width - vStride) < height / 2) {
      --numVBoxes;
    }
    if (imgSize.width - (numHBoxes - 1) * (width - hStride) < width / 2) {
      --numHBoxes;
    }
    for (int y = 0; y < numVBoxes; ++y) {
      for (int x = 0; x < numHBoxes; ++x) {
        int x0 = x * hStride;
        int y0 = y * vStride;
        int w = (x0 + width) <= imgSize.width ? width : imgSize.width - x0;
        int h = (y0 + height) <= imgSize.height ? height : imgSize.height - y0;
        bBoxes.push_back(cv::Rect(x0, y0, w, h));
      }
    }
  }

  return bBoxes;
}
