#ifndef RF_RANDOMFOREST_H
#define RF_RANDOMFOREST_H

#include <opencv2/opencv.hpp>
#include <vector>

class RandomForest {
public:
  RandomForest(int treeCount, int maxDepth, int CVFolds, int minSampleCount,
               int maxCategories);

  ~RandomForest();

  void train(cv::Ptr<cv::ml::TrainData> data, bool verbose = false);

  std::pair<float, float> predict(cv::InputArray samples,
                                  cv::OutputArray results = cv::noArray());

  void save(const cv::String &path);
  RandomForest static *load(const cv::String &path);

private:
  int mTreeCount;
  int mMaxDepth;
  int mCVFolds;
  int mMinSampleCount;
  int mMaxCategories;

  RandomForest(){};

  // M-Trees for constructing thr forest
  std::vector<cv::Ptr<cv::ml::DTrees>> mTrees;

  cv::Ptr<cv::ml::TrainData> static randomizeTreeTrainData(
      cv::Ptr<cv::ml::TrainData> data);
};

#endif // RF_RANDOMFOREST_H
