#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <opencv2/core/persistence.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "lib/HOGDescriptor.h"
#include "lib/RandomForest.h"
#include "lib/utils.h"

typedef std::vector<std::pair<cv::Rect, float>> DetectObjectsProposals;
typedef std::map<int, DetectObjectsProposals> DetectObjectsOuput;

DetectObjectsProposals nonMaxSuppress(DetectObjectsProposals proposals,
                                      float threshold) {
  DetectObjectsProposals initial = proposals;
  DetectObjectsProposals result;

  while (!initial.empty()) {
    auto bestIt = initial.begin();
    for (auto it = initial.begin(); it != initial.end(); ++it) {
      if (it->second > bestIt->second) {
        bestIt = it;
      }
    }
    result.push_back(*bestIt);
    initial.erase(bestIt);
    auto it = initial.begin();
    while (it != initial.end()) {
      cv::Rect rectIntersection = bestIt->first & it->first;
      cv::Rect rectUnion = bestIt->first | it->first;
      float iou = float(rectIntersection.area()) / float(rectUnion.area());
      if (iou > threshold) {
        it = initial.erase(it);
      } else {
        ++it;
      }
    }
  }
  return result;
}

std::string composeEvaluationOutput(DetectObjectsOuput result) {
  std::stringstream out;
  for (auto item : result) {
    int label = item.first;
    for (auto proposal : item.second) {
      auto rect = proposal.first;
      out << label << " " << rect.x << " " << rect.y << " ";
      out << rect.x + rect.width << " " << rect.y + rect.height << std::endl;
    }
  }
  return out.str();
}

DetectObjectsOuput detectObjects(cv::Mat img, cv::Ptr<RandomForest> f,
                                 float confidenceThreshold) {

  HOGDescriptor hog = createDefaultHOGDescriptor();
  std::vector<float> feats;

  auto bBoxes =
      generateBoundingBoxes(img.size(), cv::Size(80, 80), 1.4, 4, 0.32);

  DetectObjectsOuput result;

  for (auto bBox : bBoxes) {
    hog.detectHOGDescriptor(img(bBox), feats, false);
    std::pair<float, float> prediction = f->predict(feats);
    int label = prediction.first;
    float confidence = prediction.second;
    if (label == 3) {
      continue;
    }
    if (confidence >= confidenceThreshold) {
      result[label].push_back(std::pair(bBox, confidence));
    }
  }

  return result;
}

cv::Ptr<RandomForest> trainForest(cv::Ptr<cv::ml::TrainData> data, int nTrees,
                                  int maxDepth) {
  int num_classes = 4;
  cv::Ptr<RandomForest> rForest(new RandomForest(
      /**/
      nTrees,     // nTrees
      maxDepth,   // maxDepth
      1,          // CVFolds
      5,          // minSampleCount
      num_classes // maxCategories
      ));
  rForest->train(data, false);
  return rForest;
}

int main(int argc, char **argv) {

  if (std::string(argv[1]) == "train") {
    int nTrees = 25, maxDepth = 150;
    nTrees = std::atoi(argv[2]);
    maxDepth = std::atoi(argv[3]);
    auto forestOutDir = std::filesystem::path(argv[4]);

    std::cout << "Preparing train data...\n";
    auto trainData = prepareData(true, "task3");
    std::cout << "Training forest - nTrees: " << nTrees
              << ", maxDepth: " << maxDepth << std::endl;
    auto forest = trainForest(trainData, nTrees, maxDepth);
    std::cout << "Writing forest...\n";
    forest->save(forestOutDir);
    return 0;
  } else if (std::string(argv[1]) != "detect") {
    std::cout << "Unknonw command. Exiting.\n";
    return 0;
  }

  cv::String testDataDir = getDataFilePath("task3/test");
  bool writeOutput = false;
  std::filesystem::path outDir = "";
  float confidenceThreshold = 0.5;
  std::filesystem::path forestDir = argv[2];
  confidenceThreshold = std::atof(argv[3]);
  if (argc == 5) {
    writeOutput = true;
    outDir = std::filesystem::path(argv[4]);
    std::filesystem::create_directory(outDir);
  }

  std::cout << "Loading forest: " << forestDir << std::endl;
  cv::Ptr<RandomForest> forest(RandomForest::load(forestDir));
  std::cout << "Detecting...\n";

  for (const auto &imgPath : std::filesystem::directory_iterator(testDataDir)) {
    auto img = cv::imread(imgPath.path(), cv::IMREAD_COLOR);
    DetectObjectsOuput detects =
        detectObjects(img, forest, confidenceThreshold);
    DetectObjectsOuput result;

    cv::Mat vis = img.clone();
    for (auto item : detects) {
      int label = item.first;
      cv::Scalar color(255 * (0 == label), 255 * (1 == label),
                       255 * (2 == label));
      auto filtered = nonMaxSuppress(item.second, 0.01);
      for (auto proposal : filtered) {
        result[label].push_back(proposal);
        cv::rectangle(vis, proposal.first, color);
        cv::putText(vis,
                    "class: " + std::to_string(label) + " " +
                        std::to_string(proposal.second),
                    cv::Point(proposal.first.x, proposal.first.y - 5),
                    cv::FONT_HERSHEY_SIMPLEX, 0.45, color, 1); // class 0 text
      }
    }

    if (!writeOutput) {
      continue;
    }
    auto imgOutPath = outDir / imgPath.path().filename();
    cv::imwrite(imgOutPath, vis);
    auto txtOutPath = outDir / imgPath.path().filename().concat(".txt");
    std::ofstream txtOutFile(txtOutPath);
    txtOutFile << composeEvaluationOutput(result);
  }

  return 0;
}
