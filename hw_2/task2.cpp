#include <iostream>
#include <opencv2/opencv.hpp>

#include "lib/HOGDescriptor.h"
#include "lib/RandomForest.h"
#include "lib/utils.h"

struct Task2Data {
  cv::Ptr<cv::ml::TrainData> train, test;
};

template <class ClassifierType>
void evalPerformance(cv::Ptr<ClassifierType> classifier,
                     cv::Ptr<cv::ml::TrainData> data) {
  cv::Mat predictions;
  int nCorrect = 0, nWrong = 0, nSamples = data->getNSamples();
  classifier->predict(data->getSamples(), predictions);

  for (int row = 0; row < data->getResponses().rows; ++row) {
    int groundTruth = int(data->getResponses().at<float>(row, 0));
    int prediction = round(predictions.at<float>(row, 0));
    prediction == groundTruth ? ++nCorrect : ++nWrong;
  }

  std::cout << "Num samples: " << nSamples << std::endl;
  std::cout << "Num correct: " << nCorrect << std::endl;
  std::cout << "Num wrong: " << nWrong << std::endl;
  std::cout << "Accuracy: " << float(nCorrect) / float(nSamples) << std::endl;
};

void testDTrees(struct Task2Data data) {

  int num_classes = 6;
  /*
    * Create your data (i.e Use HOG from task 1 to compute the descriptor for
    your images)
    * Train a single Decision Tree and evaluate the performance
    * Experiment with the MaxDepth parameter, to see how it affects the
    performance
  */

  cv::Ptr<cv::ml::DTrees> tree = cv::ml::DTrees::create();
  tree->setCVFolds(1);
  tree->setMaxCategories(num_classes);
  tree->setMaxDepth(100);
  tree->setMinSampleCount(3);
  tree->train(data.train);

  std::cout << "\nDecision tree performance on the train set.\n";
  evalPerformance<cv::ml::DTrees>(tree, data.train);
  std::cout << "\nDecision tree performance on the test set.\n";
  evalPerformance<cv::ml::DTrees>(tree, data.test);
}

void testForest(struct Task2Data data) {

  int num_classes = 6;
  /*
    * Create your data (i.e Use HOG from task 1 to compute the descriptor for
    your images)
    * Train a Forest and evaluate the performance
    * Experiment with the MaxDepth & TreeCount parameters, to see how it affects
    the performance
  */

  cv::Ptr<RandomForest> rForest(new RandomForest(
      /**/
      25,         // nTrees
      100,        // maxDepth
      1,          // CVFolds
      3,          // minSampleCount
      num_classes // maxCategories
      ));
  rForest->train(data.train, false);

  std::cout << "\nRandom forest performance on the train set.\n";
  evalPerformance<RandomForest>(rForest, data.train);
  std::cout << "\nRandom forest performance on the train set.\n";
  evalPerformance<RandomForest>(rForest, data.test);
}

int main() {
  struct Task2Data data;
  std::cout << "Preparing data...\n";
  data.train = prepareData(true, "task2");
  data.test = prepareData(false, "task2");
  std::cout << "Testing tree...\n";
  testDTrees(data);
  std::cout << "Testing forest...\n";
  testForest(data);
  return 0;
}
