import sys
import os
import glob
import matplotlib.pyplot as plt
import numpy as np



class Stats():

    def __init__(self):
        self.num_correct = 0
        self.num_predicted = 0
        self.num_gt = 0

    def add(self, other):
        self.num_correct += other.num_correct
        self.num_predicted += other.num_predicted
        self.num_gt += other.num_gt

    def __str__(self):
        return ("num_correct: " + str(self.num_correct) + ", num_predicted: " +
                str(self.num_predicted) + ", num_gt: " + str(self.num_gt))


class Rect():

    def __init__(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def area(self):
        return (self.x1 - self.x0 + 1) * (self.y1 - self.y0 + 1)

    def intersection_over_union(self, other):
        xA = max(self.x0, other.x0)
        yA = max(self.y0, other.y0)
        xB = min(self.x1, other.x1)
        yB = min(self.y1, other.y1)
        intersection = max(0, xB - xA + 1) * max(0, yB - yA + 1)
        # print(self.x0, self.y0, self.x1, self.y1)
        # print(other.x0, other.y0, other.x1, other.y1)
        # print()
        if intersection == 0:
            return 0

        selfArea = self.area()
        otherArea = other.area()
        iou = intersection / float(selfArea + otherArea - intersection)
        return iou


def get_stats(gt, test):
    result = Stats()
    for label in gt.keys():
        gt_rectangles = gt[label]
        result.num_gt += len(gt_rectangles)
        if label not in test:
            continue
        test_rectangles = test[label]
        result.num_predicted += len(test_rectangles)
        for test_rect in test_rectangles:
            correct = False
            for gt_rect in gt_rectangles:
                if test_rect.intersection_over_union(gt_rect) > 0.5:
                    correct = True
            if correct:
                result.num_correct += 1
    return result


def parse_file(fpath):
    result = {}
    with open(fpath, 'r') as f:
        lines = f.readlines()
        for l in lines:
            parts = l.split(' ')
            parts = [int(p) for p in parts]
            rect = Rect(parts[1], parts[2], parts[3], parts[4])
            label = parts[0]
            if label not in result:
                result[label] = []
            result[label].append(rect)
    return result

def get_matching_test_fname(gt_fname, all_test_fnames):
    parts = gt_fname.split('.')
    for fname in all_test_fnames:
        if not fname.startswith(parts[0]):
            continue
        if not fname.endswith('.txt'):
            continue
        return fname


def evalueate_run(gt_cache, test_dir):
    all_test_fnames = os.listdir(test_dir)
    full_stats = Stats()
    for gt_fname in gt_cache.keys():
        gt_data = gt_cache[gt_fname]

        test_fname = get_matching_test_fname(gt_fname, all_test_fnames)
        test_fpath = os.path.join(test_dir, test_fname)
        test_data = parse_file(test_fpath)

        img_stats = get_stats(gt_data, test_data)
        full_stats.add(img_stats)
    return full_stats


def build_gt_cache(gt_dir):
    cache = {}
    for gt_fname in os.listdir(gt_dir):
        if not gt_fname.endswith('.gt.txt'):
            continue
        gt_fpath = os.path.join(gt_dir, gt_fname)
        gt_data = parse_file(gt_fpath)
        cache[gt_fname] = gt_data
    return cache


def get_pr(all_stats):
    precision = []
    recall = []
    # for key in sorted(all_stats.keys()):
    #     s = all_stats[key]
    #     precision.append(float(s.num_correct) / float(s.num_predicted))
    #     recall.append(float(s.num_correct) / float(s.num_gt))
    for s in all_stats.values():
        if s.num_predicted > 0:
            precision.append(float(s.num_correct) / float(s.num_predicted))
        else:
            precision.append(0)
        recall.append(float(s.num_correct) / float(s.num_gt))
    recall, precision = zip(*sorted(zip(recall, precision)))
    return precision, recall


def plot_pr(precision, recall):
    plt.ylabel('precision')
    plt.xlabel('recall')
    plt.title('PR Curve')
    plt.plot(recall, precision)
    plt.show()


def main():
    gt_dir = sys.argv[1]
    test_dir_pattern = sys.argv[2]
    gt_cache = build_gt_cache(gt_dir)
    all_stats = {}
    for test_dir in glob.glob(test_dir_pattern+'*'):
        key = int(test_dir[-2:])
        all_stats[key] = evalueate_run(gt_cache, test_dir)

    precision, recall = get_pr(all_stats)
    plot_pr(precision, recall)


if __name__ == '__main__':
    main()
