#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc != 2) {
    printf("usage: DisplayImage.out <Image_Path>\n");
    return -1;
  }
  cv::Mat image;
  image = cv::imread(argv[1], 1);
  if (!image.data) {
    printf("No image data \n");
    return -1;
  }
  cv::namedWindow("Display Image", cv::WINDOW_GUI_NORMAL);
  cv::resizeWindow("Display Image", 1600, 900);
  cv::imshow("Display Image", image);
  return 0;
}
