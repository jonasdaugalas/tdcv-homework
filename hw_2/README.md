# Get started

* Modify data path `DATA_PATH` in [lib/utils.h]:
* Build:
```
cd build
cmake ..
make
# should produce executables 'task1', 'task2' [, 'task3']
```
